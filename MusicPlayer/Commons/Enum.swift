//
//  Enum.swift
//  MusicPlayer
//
//  Created by Alvaro IDJOUBAR on 12/05/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation
import UIKit

enum PlayingState {
    case playing
    case pause
    
    var image: UIImage {
        switch self {
            case .playing: return UIImage(named: "pauseIcon")!
            case .pause: return UIImage(named: "playIcon")!
        }
    }
}
