//
//  MyViewController.swift
//  MusicPlayer
//
//  Created by Alvaro IDJOUBAR on 11/05/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class MyViewController<V: MyView>: UIViewController {

    override func loadView() {
        view = V()
    }

    var customView: V {
        return view as! V
    }
}
