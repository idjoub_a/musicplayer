//
//  PlayerViewController.swift
//  MusicPlayer
//
//  Created by Alvaro IDJOUBAR on 11/05/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import AVFoundation

class PlayerViewController: MyViewController<PlayerView> {

    var counter = 1
    var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customView.delegate = self
        computeAudioPlayer()
    }
    
    func computeAudioPlayer() {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf:
                URL.init(fileURLWithPath: Bundle.main.path(forResource: "sample\(counter)", ofType: "mp3")!))
            
            audioPlayer.prepareToPlay()
            
            let audioSession = AVAudioSession.sharedInstance()
            
            do
            {
                try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            }
            catch {
                print("audioSession error :: \(error)")
            }
        }
        catch {
            print("audioPlayer error :: \(error)")
        }
    }
}

extension PlayerViewController: PlayerViewDelegate {

    func playButtonView(_ view: PlayerView, didTapPlayButton: UIButton) {
        print("playButton Tapped")

        if view.playingState == PlayingState.pause {
            view.playingState = PlayingState.playing
            audioPlayer.play()
        }
        else {
            view.playingState = PlayingState.pause
            audioPlayer.pause()
        }
    }
    
    func likeButtonView(_ view: PlayerView, didTapLikeButton: UIButton) {
        print("likeButton Tapped")
    }
    
    func dislikeButtonView(_ view: PlayerView, didTapDislikeButton: UIButton) {
        print("dislikeButton Tapped")

        if counter < 4 {
            self.counter += 1
            computeAudioPlayer()
            
            if view.playingState == PlayingState.pause {
                view.playingState = PlayingState.playing
            }
            audioPlayer.play()
        }
    }
}

