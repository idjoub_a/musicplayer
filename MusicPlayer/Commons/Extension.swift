//
//  Extension.swift
//  MusicPlayer
//
//  Created by Alvaro IDJOUBAR on 11/05/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

extension UIButton {
    convenience init(type: UIButtonType = .system,
                     title: String = "",
                     image: UIImage?) {
        
        self.init(type: type)
        
        self.setTitle(title, for: .normal)
        self.setImage(image, for: .normal)
    }
}

extension UIView {
    func rotateAnimation() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Double.pi * 2
        rotationAnimation.duration = 3
        rotationAnimation.repeatCount = .infinity
        self.layer.add(rotationAnimation, forKey: nil)
    }
    
    func pauseAnimation() {
        let pausedTime: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
    }
    
    func resumeAnimation() {
        let pausedTime: CFTimeInterval = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        let timeSincePause: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
    }
    
    func flash() {
        UIView.animate(withDuration: 2,
                       delay: 0,
                       usingSpringWithDamping: 0.20,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: {
                        self.transform = .identity
        },
                       completion: { finished in
                        self.flash()
        })
    }
}
