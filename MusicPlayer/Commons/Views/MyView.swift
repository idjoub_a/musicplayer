//
//  MyView.swift
//  MusicPlayer
//
//  Created by Alvaro IDJOUBAR on 11/05/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class MyView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setViews()
        layoutViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setViews()
        layoutViews()
    }
    
    /// Set your view and its subviews here.
    func setViews() {
        backgroundColor = .white
    }
    
    /// Layout your subviews here.
    func layoutViews() {}
}
