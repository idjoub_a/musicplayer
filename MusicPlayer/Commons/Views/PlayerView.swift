//
//  PlayerView.swift
//  MusicPlayer
//
//  Created by Alvaro IDJOUBAR on 11/05/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import SnapKit

protocol PlayerViewDelegate: class {
    func playButtonView(_ view: PlayerView, didTapPlayButton: UIButton)
    func likeButtonView(_ view: PlayerView, didTapLikeButton: UIButton)
    func dislikeButtonView(_ view: PlayerView, didTapDislikeButton: UIButton)
}

class PlayerView: MyView {
    
    weak var delegate: PlayerViewDelegate?
    static var isUserListeningSong: Bool = false

    var playingState = PlayingState.pause {
        didSet {
            if playingState == .pause { PlayerView.isUserListeningSong = true }
            setPlayPauseButtonImage()
        }
    }
    
    private lazy var timeSlider = UISlider()
    private lazy var diskView = UIImageView(image: UIImage(named: "vinylIcon"))
    private lazy var deleteButton = UIButton(image: UIImage(named: "deleteIcon"))
    private lazy var playPauseButton = UIButton(image: playingState.image)
    private lazy var likeButton = UIButton(image: UIImage(named: "likeIcon"))
    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [deleteButton, playPauseButton, likeButton])
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        return stackView
    }()
    
    private lazy var popLikeMessage = UILabel()
    
    private lazy var screenStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [popLikeMessage, diskView, timeSlider])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        return stackView
    }()
    
    override func setViews() {
        super.setViews()
        backgroundColor = .brown

        deleteButton.tintColor = .black
        deleteButton.addTarget(self, action: #selector(didTapDislikeButton(_:)), for: .touchUpInside)

        playPauseButton.tintColor = .black
        playPauseButton.contentVerticalAlignment = .fill
        playPauseButton.contentHorizontalAlignment = .fill
        playPauseButton.addTarget(self, action: #selector(didTapPlayButton(_:)), for: .touchUpInside)

        likeButton.tintColor = .black
        likeButton.addTarget(self, action: #selector(didTapLikeButton(_:)), for: .touchUpInside)

        addSubview(diskView)
        addSubview(buttonStackView)
        addSubview(screenStackView)
    }
    
    override func layoutViews() {
        
        diskView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(200)
            make.width.equalTo(200)
        }
        
        buttonStackView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(40)
            make.trailing.equalToSuperview().inset(40)
            make.bottom.equalToSuperview().inset(40)
        }
        
        playPauseButton.snp.makeConstraints { make in
            make.height.equalTo(70)
            make.width.equalTo(70)
        }
        
        screenStackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(40)
            make.leading.equalToSuperview().inset(40)
            make.trailing.equalToSuperview().inset(40)
            make.bottom.equalTo(buttonStackView.snp.top)
        }
        
        timeSlider.snp.makeConstraints { make in
            make.leading.equalTo(screenStackView)
            make.trailing.equalTo(screenStackView)
            make.bottom.equalTo(screenStackView)
            make.height.equalTo(20)
        }
    }
    
    private func setPlayPauseButtonImage() {
        playPauseButton.setImage(playingState.image, for: .normal)
    }
}

private extension PlayerView {
    
    func shouldLaunchRotateAnimation() {
        if PlayerView.isUserListeningSong == false { // Called when first launch
            diskView.rotateAnimation()
        }
        playingState == PlayingState.pause ? pauseAnimation() : resumeAnimation()
    }
    
    @objc func didTapPlayButton(_ button: UIButton) {
        delegate?.playButtonView(self, didTapPlayButton: button)
        shouldLaunchRotateAnimation()
    }
    
    @objc func didTapLikeButton(_ button: UIButton) {
        delegate?.likeButtonView(self, didTapLikeButton: button)
        popLikeMessage.text = "You Kâtch this song"
        //popLikeMessage.flash()
        button.flash()
    }
    
    @objc func didTapDislikeButton(_ button: UIButton) {
        delegate?.dislikeButtonView(self, didTapDislikeButton: button)
        shouldLaunchRotateAnimation()
    }
}
